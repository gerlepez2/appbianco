﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using Login.Services;
using System.IO;
using Login.Droid.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(ISQLite))]

namespace Login.Droid.Services
{
    public class SQLite : ISQLite
    {

        public SQLiteConnection GetConnection()
        {
            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "prueba.db");
            return new SQLiteConnection(path);
        }
    }
}