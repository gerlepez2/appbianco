﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using System.IO;
using Login.Services;
using Login.UWP.Services;
using Windows.Storage;
[assembly: Xamarin.Forms.DependencyAttribute(typeof(Login.UWP.Services.SQLite))]
namespace Login.UWP.Services
{
        public class SQLite : ISQLite
        {
            private string GetPath()
            {
                var dbName = "prueba.db";
                var path = Path.Combine(ApplicationData.Current.LocalFolder.Path, dbName);
                return path;
            }
            public SQLiteConnection GetConnection()
            {
                return new SQLiteConnection(GetPath());
            }
            public SQLiteAsyncConnection GetConnectionAsync()
            {
                return new SQLiteAsyncConnection(GetPath());
            }
        }
    
}
