﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using SQLite;
using Login.iOS.Services;
using Login.Services;
using System.IO;
[assembly: Xamarin.Forms.Dependency(typeof(ISQLite))]
namespace Login.iOS.Services
{
    public class SQLite : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop), "prueba.db");
            return new SQLiteConnection(path);
        }

    }
}