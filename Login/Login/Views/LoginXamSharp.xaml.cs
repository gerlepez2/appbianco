﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Login
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginXamSharp : ContentPage
	{
        public static string NombreUsuario = "";
        public static string Contrasenia = "";
        

        ILoginManager iml = null;

		public LoginXamSharp (ILoginManager ilm)
		{
			InitializeComponent ();
            iml = ilm;
		}

        async void btnLoginClick (object sender, EventArgs e)
        {
            NombreUsuario = Username.Text;
            Contrasenia = Password.Text;
            if (NombreUsuario == "David" && Contrasenia == "123")
            {
                App.Current.Properties["Name"] = NombreUsuario;
                App.Current.Properties["IsLoggedIn"] = true;
                iml.ShowMainPage();
            }
            else
            {
                await DisplayAlert("", "Credenciales Incorrectas", "Ok");
            }

        }
	}
}