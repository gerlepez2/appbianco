﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace Login.Services
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
