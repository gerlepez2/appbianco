﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Login.Services
{
    public class UserRepository
    {
        private readonly DataBaseContext dataBaseContext;
        private static object locker = new object();

        public UserRepository(DataBaseContext dataBaseContext)
        {
            this.dataBaseContext = dataBaseContext;
        }

        public UserRepository()
        {

        }

        public User user => GetUser();


        public User GetUser()
        {
            lock (locker)
            {
                using (var connection = dataBaseContext.GetConnection())
                {
                    var stock = connection.Get<User>(1);
                    return stock;
                }
            }
        }
    }
}
