﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using Xamarin.Forms;

namespace Login.Services
{
    public class DataBaseContext
    {
        public bool CreateDataBase()
        {
            try
            {
                using (var connection = GetConnection())
                {
                    //Debes crear aquí cada una de las tablas que necesites
                    connection.CreateTable<User>();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public SQLiteConnection GetConnection()
        {

            //Obtienes la implementación del contrato
            return DependencyService.Get<ISQLite>().GetConnection();
        }
    }
}
